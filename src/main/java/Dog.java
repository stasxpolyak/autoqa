public class Dog extends Pet{
    Dog(){}
    Dog(String name, String color, HOMESTATUS homestatus){
        super(name, color, homestatus);
    }
    Dog(Dog dog){
        this.name = dog.name;
        this.color = dog.color;
        this.homestatus = dog.homestatus;
    }

    public String getType(){
        return "Dog";
    }
}
