public abstract class Pet {
    String name;
    String color;
    enum HOMESTATUS {
        Home("Домашний питомец"),
        Homeless("Бездомный питомец");
        private String status;
        HOMESTATUS(String status){
            this.status = status;
        }
    }
    public HOMESTATUS homestatus;
    Pet(){}
    Pet(String name, String color, HOMESTATUS homestatus){
        this.name = name;
        this.color = color;
        this.homestatus = homestatus;
    }

    Pet(Pet pet){
        this.name = pet.name;
        this.color = pet.color;
        this.homestatus = pet.homestatus;
    }

    public String getType(){
        return "Pet";
    }

    @Override
    public String toString(){
        return this.getType()+" "+this.name+", color: "+this.color;
    }
}
