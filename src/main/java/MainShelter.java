public class MainShelter {
    public static void main(String[] args) {
        Shelter shelter = new Shelter();
        Cat cat1 = new Cat("Kesha", "red", Pet.HOMESTATUS.Home);
        Cat cat2 = new Cat("Matroskin", "white", Pet.HOMESTATUS.Homeless);
        Cat cat3 = new Cat("Barsik", "orange", Pet.HOMESTATUS.Homeless);
        Dog dog1 = new Dog("Rex","black", Pet.HOMESTATUS.Homeless);
        Dog dog2 = new Dog("Bobik","white", Pet.HOMESTATUS.Homeless);
        Dog dog3 = new Dog("Sharik","grey", Pet.HOMESTATUS.Homeless);
        shelter.addPet(cat1);
        shelter.addPet(cat2);
        shelter.addPet(cat3);
        shelter.addPet(dog1);
        shelter.addPet(dog2);
        shelter.addPet(dog3);
        //shelter.print();
        //System.out.println(shelter.getRandomPet());
        //System.out.println(shelter.getRandomPet());
        //System.out.println(shelter.getRandomPet());
        //System.out.println(shelter.getRandomPet());
        System.out.println("=====================");
        //shelter.print();
        System.out.println(shelter.getPet("Dog","white"));
        System.out.println("====================");
        shelter.print();
    }
}
