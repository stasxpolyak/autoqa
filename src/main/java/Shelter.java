import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Shelter {
    private List<Pet> shelter = new ArrayList<Pet>();

    public void print(){
        if (!shelter.isEmpty()){
            for(Pet pet: shelter){
                System.out.println(pet);
            }
        }
    }

    public int getSize(){
        return shelter.size();
    }

    public boolean isEmpty(){
        return shelter.isEmpty();
    }

    public void addPet(Pet pet){
        if (pet.homestatus == Pet.HOMESTATUS.Homeless){
            shelter.add(pet);
        }
    }

    public Pet getRandomPet() {
        if (!shelter.isEmpty()){
            Random random = new Random();
            int index = random.nextInt(shelter.size());
            Pet pet = shelter.get(index);
            shelter.remove(index);
            return pet;
        }
        return null;
    }

    public Pet getPet(String type, String color){
        Pet temp = null;
        if (!shelter.isEmpty()) {
            for (Pet pet : shelter) {
                if ((pet.color == color) && (pet.getType() == type)) {
                    temp = pet;
                    break;
                }
            }
            shelter.remove(temp);
            return temp;
        }
        return temp;
    }
}
