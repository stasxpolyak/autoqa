public class Cat extends Pet{
    Cat(){}
    Cat(String name, String color, HOMESTATUS homestatus)
    {
        super(name, color, homestatus);
    }
    Cat(Cat cat){
        this.name = cat.name;
        this.color = cat.color;
        this.homestatus = cat.homestatus;
    }
    public String getType(){
        return "Cat";
    }
}
